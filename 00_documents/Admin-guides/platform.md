# Platform Installation
This Platform Installation document will guide through the configuration and rollout of the urban dataspace platform components on a kubernetes cluster.

If you do not have a running kubernetes environment please refer to the [Cluster Installation](cluster.md) guide.

## Installation
The installation will be done by preparing a configuration file and running an ansible playbook.

### Requirements
A few requirements have to be met/prepared in order to create the installation process.
- A Linux server with a running kubernetes. We advice to use an up-to-date kubernetes installation.
- An ingress controller installed on the cluster. Preferably [nginx-ingress](https://github.com/kubernetes/ingress-nginx).
- Either a general admin kubeconfig with full access rights, or a selection of kubeconfigs provided by your hoster to deploy components into your desired namespaces.
> It is advised to create the configuration (inventory) first, so you get an understanding which namespace(s) you maybe need to have prepared by the k8s-hoster and which additional information you need.

<br>**Note:** For the configuration of the platform there will come up additional requirements those are based on the use case and the purpose of the installation. One example:
<ul>
    For a platform on a (near-)production cluster, where users shall be able to create accounts it is strongly advised to use a functional SMTP eMail-server. On the other hand on a development cluster, or on your first steps with platform you may not need this functionality.
</ul>

<br>As for the machine executing the ansible playbook the following requirements have to be met.
- curl
- Any [git](https://git-scm.com/) client
- sshpass
- openssl
- Python >= 3.9
    - ansible.core >= 2.13
    - openshift 0.12.1
    - kubernetes 12.0.1 
    - ansible-lint >= 5.0 
    - docker >= 5.0 
    - jmespath
    - pymongo==3.12.2
    - setuptools
- Helm
- Ansible Galaxy Module: kubernetes.core >= 2.2
- Ansible Galaxy Module: community.grafana
- Ansible Galaxy Module: community.mongodb:==1.3.2

### Configuration
Please refer to the [inventory configuration guide](inventory-guide.md) for a complete documentation about the possible configuration.<br>
Most parts of the [configuration-file](../../02_core_platform/default_inventory.yml) is clear on how to fill. Refer to the guide if in doubt, or if you want to see an example on the format.

### Rollout
1. Checkout this repository
```bash
git clone {REPO_URL}
cd core-platform
git submodule update --init --recursive
```
2. Place the created `inventory` somewhere accessible.
3. Run the playbook

Full Installation: 
```bash
ansible-playbook -l localhost -i inventory 02_core_platform/full_install.yml 
```

### Component Update/Change
It is possible to instruct the playbook to run only portions of the script, if you made changes to the inventory or any deployment(i.e. version update).<br>
For each component, or group of components there are tags placed in the [playbook file](../../02_core_platform/full_install.yml)

For example the Postgres operator, or Keycloak: 
```yaml
    - name: Setup Postgres Operator
      import_tasks: tasks/01_op_postgres_operator.yml
      when: inv_op_stack.postgres_operator.enable
      tags: [ 'operators', 'operator_postgres' ]
...
    - name: Setup Keycloak Client
      import_tasks: tasks/04_idm_keycloak.yml
      tags: [ 'idm_keycloak' ]
```
The `operators` tag is used for all scripts which will update/deploy operators to the cluster. The tags `operator_postgres` or `idm_keycloak` would run all scripts with regard to these components.

The command for running only the scripts for the Postgres operator and Keycloak client, would look like this:
```bash
ansible-playbook -l localhost -i inventory 02_core_platform/full_install.yml --tags operator_postgres,idm_keycloak
```

It is possible to use a similiar mechanic to omit components, by using the `--skip-tags` option:
```bash
ansible-playbook -l localhost -i inventory 02_core_platform/full_install.yml --skip_tags ml_grafana,ml_loki_promtail,cm_frost
```
