# Urban Data Platform in Kubernetes

> This version of the platform is currently under development and actively maintained.

This repository provides you an automated setup for an Urban Data Space Platform within a Kubernetes environment.

If you are interested in any topic, please find more detailed information in the following documents:

## Setup Documentation
* [Cluster-Installation](00_documents/Admin-guides/cluster.md)
* [Platform-Installation](00_documents/Admin-guides/platform.md)
* [Current architecture diagram](00_documents/Admin-guides/img/architecture.png)

## Component Documentation
* [IDM - Keycloak](00_documents/keycloak/README.md)
* [APIM - Gravitee](00_documents/gravitee/README.md)

## License
This work is licensed under [EU PL 1.2](LICENSE) by the State of Berlin, Germany, represented by [Tegel Projekt GmbH](https://www.tegelprojekt.de/) and by the City of Paderborn. 

All contributions to this repository from January 1st 2020 on are considered to be licensed under the EU PL 1.2 or any later version.
This project doesn't require a CLA (Contributor License Agreement). The copyright belongs to all the individual contributors.
