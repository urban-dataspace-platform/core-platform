FROM willhallonline/ansible:2.10-ubuntu-20.04

COPY requirements.yml requirements.yml

RUN apt-get update -y && apt-get install curl apt-transport-https -y
# Add Helm package repo
RUN curl https://baltocdn.com/helm/signing.asc | apt-key add -
RUN echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
# Add kubectl package repo
RUN curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
# Install required system packages
RUN apt-get update -y && apt install openssh-client curl vim git less iputils-ping sshpass libpq-dev gcc python3-dev musl-dev helm kubectl -y
# Install required python packages
RUN python3 -m pip install jmespath docker psycopg2-binary 'pymongo==3.12.1' 'openshift==0.12.1'
RUN ansible-galaxy install -r requirements.yml

# remove copied file, so before_script wont cause any issues when executing the ci pipeline
RUN rm requirements.yml

CMD ["/bin/bash"]
