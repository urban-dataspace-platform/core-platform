---
# file: 03_setup_k8s_platform/tasks/context_management-stack/install_mongodb.yml

# - name: Read MongoDB values
#   ansible.builtin.include_vars:
#     file: context_management-stack/mongodb.yml
#     name: mongodb

- name: Deploy MongoDB v3.6
  no_log: '{{ ansible_debug.no_log }}'
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    definition: "{{ item }}"
  loop:
    - "{{ lookup('template', 'templates/08_context_management/mongodb.yml') | from_yaml_all | list }}"

- name: Wait till deployment of MongoDB v3.6 is completed
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    kind: Pod
    label_selectors:
      - statefulset.kubernetes.io/pod-name=orion-mongodb-0
      - app=orion-mongodb
    wait: yes
    wait_sleep: 10
    wait_timeout: 360

# To add MongoDB user for Orion we need to run kubectl port-forward
- name: Set path of PID file for kubectl port-forward
  ansible.builtin.set_fact:
    kubectl_pid_file: "/tmp/kubectl_pid"

- name: Stop kubectl port-forward to MongoDB service
  ansible.builtin.shell: |
    kill "$(cat {{ kubectl_pid_file|quote }})" || true
    rm -f {{ kubectl_pid_file | quote }}
  args:
    executable: /bin/bash
    removes: "{{ kubectl_pid_file }}"

- name: Remove old PID files before running kubectl port-forward
  ansible.builtin.file:
    path: "{{ kubectl_pid_file }}"
    state: absent

- name: Run kubectl port-forward to MongoDB service
  ansible.builtin.shell: |
    nohup kubectl --kubeconfig="{{ k8s_config }}" port-forward --namespace {{ k8s_namespace }} svc/orion-mongodb 27017:27017 </dev/null >/dev/null 2>&1 &
    echo "$!" >{{ kubectl_pid_file | quote }}
  args:
    executable: /bin/bash
    creates: "{{ kubectl_pid_file }}"

- name: Check connection to MongoDB
  community.mongodb.mongodb_info:
    login_user: "{{ tenant.fiware.mongo.initdb_root_username }}"
    login_password: "{{ tenant.fiware.mongo.initdb_root_password }}"
    login_host: "127.0.0.1"
    login_port: "27017"
  register: mongodb_info
  retries: 5
  delay: 10
  until: mongodb_info is succeeded

- name: Fail if connection to MongoDB cannot be established
  fail:
    msg: "Unable to connect to MongoDB. Please, check deployment of MongoDB!"
  when: mongodb_info is failed

- name: Create MongoDB User for Orion
  community.mongodb.mongodb_user:
    state: present
    login_user: "{{ tenant.fiware.mongo.initdb_root_username }}"
    login_password: "{{ tenant.fiware.mongo.initdb_root_password }}"
    login_host: "127.0.0.1"
    login_port: "27017"
    database: "{{ tenant.fiware.mongo.initdb_database }}"
    name: "{{ tenant.fiware.orion.mongodb_user }}"
    password: "{{ tenant.fiware.orion.mongodb_password }}"
    roles:
      - { db: "{{ tenant.fiware.mongo.initdb_database }}", role: "dbOwner" }

- name: Stop kubectl port-forward to MongoDB service
  ansible.builtin.shell: |
    kill "$(cat {{ kubectl_pid_file|quote }})" || true
    rm -f {{ kubectl_pid_file | quote }}
  args:
    executable: /bin/bash
    removes: "{{ kubectl_pid_file }}"
