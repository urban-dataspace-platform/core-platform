- name: Set skip to false
  set_fact:
    skip: false

- name: Get superuser credentials for {{ item.name }}
  kubernetes.core.k8s_info:
    namespace: '{{ item.ns }}'
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    kind: Secret
    name: "postgres.{{ item.service }}.credentials.postgresql.acid.zalan.do"
  register: superuser_credentials

- name: Check if superuser credentials are available
  debug:
    msg: "Secret 'postges.{{ item.service }}.credentials.postgresql.acid.zalan.do' not found in namespace '{{ item.ns }}'"
  when: superuser_credentials.resources | length == 0

- name: Set skip to true if superuser credentials are not available
  set_fact:
    skip: true
    postgres_master_instance_resources: []
  when: superuser_credentials.resources | length == 0

- name: Store superuser credentials
  set_fact:
    superuser_username: "{{ superuser_credentials.resources[0].data.username | b64decode }}"
    superuser_password: "{{ superuser_credentials.resources[0].data.password | b64decode }}"
    cacheable: yes
  when: not skip

- name: Find postgres master instance. 
  kubernetes.core.k8s_info:
    namespace: '{{ item.ns }}'
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    kind: Pod
    label_selectors:
      - spilo-role=master
  register: postgres_master_instance
  when: not skip

- name: Set postgres_master_instance_resources
  set_fact:
    postgres_master_instance_resources: "{{ postgres_master_instance.resources }}"
  when: not skip

- name: Fail if more than one master is found
  fail:
    msg: "More than one master instance found in namespace '{{ item.ns }}'. This issue must be resolved, as it can mean that the Postgres cluster is unhealthy."
  when: postgres_master_instance_resources | length > 1 and skip is not defined

- name: Set name of master
  set_fact:
    master_instance_name: "{{ postgres_master_instance.resources[0].metadata.name }}"
  when: not skip

- name: Create new user password for monitor user
  set_fact:
    new_user_password: "{{ lookup('ansible.builtin.password', '/tmp/passwordfile_{{item.name}}', chars=['ascii_letters', 'digits'], length=64) }}"
  when: not skip

- name: Create monitoring user
  shell: |
    kubectl --kubeconfig="{{ k8s_config }}" exec -n {{ item.ns }} {{ master_instance_name }} -c postgres -- \
    sh -c "PGPASSWORD=$PGPASSWORD_SUPERUSER psql -U postgres -d postgres -c \"CREATE USER postgres_exporter WITH PASSWORD '{{ new_user_password }}';\""
  ignore_errors: yes
  register: create_user_log
  when: not skip

- name: Check if user already exists
  debug:
    msg: "User 'postgres_exporter' already exists in database '{{ item.name }}'"
  when: create_user_log.stderr_lines is defined and create_user_log.stderr_lines is search("role \"postgres_exporter\" already exists") and not skip

- name: Set new password for monitoring user if exists
  shell: |
    kubectl --kubeconfig="{{ k8s_config }}" exec -n {{ item.ns }} {{ master_instance_name }} -c postgres -- \
    sh -c "PGPASSWORD=$PGPASSWORD_SUPERUSER psql -U postgres -d postgres -c \"ALTER USER postgres_exporter WITH PASSWORD '{{ new_user_password }}';\""
  when: create_user_log.stderr_lines is defined and create_user_log.stderr_lines is search("role \"postgres_exporter\" already exists") and not skip

- name: Set search path for postgres_exporter
  shell: |
    kubectl --kubeconfig="{{ k8s_config }}" exec -n {{ item.ns }} {{ master_instance_name }} -c postgres -- \
    sh -c "PGPASSWORD=$PGPASSWORD_SUPERUSER psql -U postgres -d postgres -c \"ALTER USER postgres_exporter SET SEARCH_PATH TO postgres_exporter,pg_catalog;\""
  when: not skip

- name: Grant connect on database to postgres_exporter
  shell: |
    kubectl --kubeconfig="{{ k8s_config }}" exec -n {{ item.ns }} {{ master_instance_name }} -c postgres -- \
    sh -c "PGPASSWORD=$PGPASSWORD_SUPERUSER psql -U postgres -d postgres -c \"GRANT CONNECT ON DATABASE postgres TO postgres_exporter;\""
  when: not skip

- name: Grant pg_monitor to postgres_exporter
  shell: |
    kubectl --kubeconfig="{{ k8s_config }}" exec -n {{ item.ns }} {{ master_instance_name }} -c postgres -- \
    sh -c "PGPASSWORD=$PGPASSWORD_SUPERUSER psql -U postgres -d postgres -c \"GRANT pg_monitor to postgres_exporter;\""
  when: not skip

- name: Add name and password to postgres_exporter_auth_modules
  set_fact:
    postgres_exporter_auth_modules: "{{ postgres_exporter_auth_modules | default({}) | combine({item.name: new_user_password}) }}"
  when: not skip

- name: Create object to append
  set_fact:
    endpoint: {'endpoint': '{{ item.service }}.{{ item.ns }}', 'name': '{{ item.name }}', 'port': '5432', 'databaseName': 'postgres'}
  when: not skip

- name: Append to helm values list postgres_endpoint_configs
  set_fact:
    postgres_endpoint_configs: "{{ postgres_endpoint_configs + [ endpoint ] }}"
  when: not skip