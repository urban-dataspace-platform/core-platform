- name: Re-Login to keycloak
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/realms/master/protocol/openid-connect/token"
    return_content: yes
    body_format: form-urlencoded
    body:
        grant_type: "password"
        client_id: "admin-cli"
        username: "{{ ADMIN_USERNAME }}"
        password: "{{ ADMIN_PASSWORD }}"
    status_code: 200
  register: keycloak_login

- name: Create Platform Admin User
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users"
    return_content: yes
    body_format: json
    body: "{{ lookup('template', '04_idm_keycloak/users/platform_admin.json.j2') }}"
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 201,409
  register: keycloak_platform_admin_response

- name: Get users
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: users

- name: "Set platform user id."
  set_fact:
    platform_user_id: "{{ users.json | json_query(query) }}"
  vars:
    query: "[?username== 'admin@{{ DOMAIN }}'].id | [0]"

- name: Get roles api_access
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/clients/{{ client_id_api_access }}/roles"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: api_access_roles

- name: Get roles grafana
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/clients/{{ client_id_grafana }}/roles"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: grafana_roles

- name: Get roles gravitee
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/clients/{{ client_id_gravitee }}/roles"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: gravitee_roles

- name: Get roles Admin Tools
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/clients/{{ client_id_admin_tools }}/roles"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: admin_tools_roles

- name: Get roles minio
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/clients/{{ client_id_minio }}/roles"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: minio_roles

- name: "Set role id api_access."
  set_fact:
    api_access_role_id: "{{ api_access_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'dataAdmin'].id | [0]"

- name: "Set role id grafana."
  set_fact:
    grafana_role_id: "{{ grafana_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'grafanaServerAdmin'].id | [0]"

- name: "Set role id gravitee."
  set_fact:
    gravitee_role_id: "{{ gravitee_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'graviteeAdmin'].id | [0]"

- name: "Set role id admin tools."
  set_fact:
    admin_tools_role_id: "{{ admin_tools_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'adminToolsAdmin'].id | [0]"

- name: "Set role id minio for minioAdmin."
  set_fact:
    minioadmin_role_id: "{{ minio_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'minioAdmin'].id | [0]"

- name: "Set role id minio for consoleAdmin."
  set_fact:
    consoleadmin_role_id: "{{ minio_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'consoleAdmin'].id | [0]"

- name: "Set role id minio for readwrite."
  set_fact:
    readwrite_role_id: "{{ minio_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'readwrite'].id | [0]"

- name: "Set role id minio for diagnostics."
  set_fact:
    diagnostics_role_id: "{{ minio_roles.json | json_query(query) }}"
  vars:
    query: "[?name== 'diagnostics'].id | [0]"


### Following configureation will be skipped if target user already exists

- name: Check if the Kubernetes Secret already exists
  kubernetes.core.k8s_info:
    kind: Secret
    kubeconfig: "{{ k8s_config }}"
    namespace: "{{ k8s_namespace }}"
    name: "{{ REALM | lower }}-keycloak-admin"
  register: user_pass_exists

- name: Generate User Password
  ansible.builtin.shell: echo "$(pwgen -c -n -s -B -N 1 10 -y -r=$\´\`\')$(pwgen -c -n -s -B -N 1 10 -y -r=$\´\`\')"
  register: pwgen_output
  when: user_pass_exists.resources | length == 0

- name: Set USER_PASSWORD to pwgen_output
  set_fact:
    USER_PASSWORD: "{{ pwgen_output.stdout }}"
  when: user_pass_exists.resources | length == 0

- name: Check USER_PASSWORD is not empty
  fail:
    msg: "USER_PASSWORD is empty. Check if pwgen is installed."
  when: USER_PASSWORD is defined and USER_PASSWORD == ""

- name: Create Kubernetes Secret
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    namespace: "{{ k8s_namespace }}"
    definition: 
      apiVersion: v1
      kind: Secret
      type: Opaque
      metadata:
        name: "{{ REALM | lower }}-keycloak-admin"
      data:
        password: "{{ USER_PASSWORD | b64encode }}"
  when: (user_pass_exists.resources | length == 0) and keycloak_platform_admin_response.status != 409

- name: Create Kubernetes Secret if admin user already exists but no K8s secret matches (useful for legacy platforms)
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    namespace: "{{ k8s_namespace }}"
    definition: 
      apiVersion: v1
      kind: Secret
      type: Opaque
      metadata:
        name: "{{ REALM | lower }}-keycloak-admin"
      data:
        password: "{{ inv_idm.platform.master_password }}"
  when: (user_pass_exists.resources | length == 0) and keycloak_platform_admin_response.status == 409

- name: Get User Password
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    kind: Secret
    namespace: "{{ k8s_namespace }}"
    name: "{{ REALM | lower }}-keycloak-admin"
  register: user_secret

- name: Store User Password
  set_fact:
    USER_PASSWORD: "{{ user_secret.resources[0].data.password | b64decode }}"
    cacheable: yes

- name: Set Password for User (from k8s secret)
  uri:
    method: PUT
    url: "{{ keycloak_platform_admin_response.location }}/reset-password"
    return_content: yes
    body_format: json
    body: |
        { 
          "type":"password",
          "value":"{{ USER_PASSWORD }}",
          "temporary": False
        }
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 204
  when: keycloak_platform_admin_response.status != 409

- name: Print the gateway for each host when defined
  ansible.builtin.debug:
    msg: "{{ platform_user_id }}"

- name: Assign Roles to Platform Admin (api-access)
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users/{{ platform_user_id }}/role-mappings/clients/{{ client_id_api_access }}"
    return_content: yes
    body_format: json
    body: |
      [{
        "id": "{{ api_access_role_id }}",
        "name": "dataAdmin",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_api_access }}"
      }]
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 204
  when: keycloak_platform_admin_response.status != 409

- name: Assign Roles to Platform Admin (grafana)
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users/{{ platform_user_id }}/role-mappings/clients/{{ client_id_grafana }}"
    return_content: yes
    body_format: json
    body: |
      [{
        "id": "{{ grafana_role_id }}",
        "name": "grafanaServerAdmin",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_grafana }}"
      }]
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 204
  when: keycloak_platform_admin_response.status != 409

- name: Assign Roles to Platform Admin (gravitee)
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users/{{ platform_user_id }}/role-mappings/clients/{{ client_id_gravitee }}"
    return_content: yes
    body_format: json
    body: |
      [{
        "id": "{{ gravitee_role_id }}",
        "name": "graviteeAdmin",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_gravitee }}"
      }]
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 204
  when: keycloak_platform_admin_response.status != 409

- name: Assign Roles to Platform Admin (Admin Tools)
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users/{{ platform_user_id }}/role-mappings/clients/{{ client_id_admin_tools }}"
    return_content: yes
    body_format: json
    body: |
      [{
        "id": "{{ admin_tools_role_id }}",
        "name": "adminToolsAdmin",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_admin_tools }}"
      }]
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 204
  when: keycloak_platform_admin_response.status != 409

- name: Assign Roles to Platform Admin (MINIO)
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users/{{ platform_user_id }}/role-mappings/clients/{{ client_id_minio }}"
    return_content: yes
    body_format: json
    body: |
      [{
        "id": "{{ minioadmin_role_id }}",
        "name": "minioAdmin",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_minio }}"
      },
      {
        "id": "{{ consoleadmin_role_id }}",
        "name": "consoleAdmin",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_minio }}"
      },
      {
        "id": "{{ readwrite_role_id }}",
        "name": "readwrite",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_minio }}"
      },
      {
        "id": "{{ diagnostics_role_id }}",
        "name": "diagnostics",
        "composite": false,
        "clientRole": true,
        "containerId": "{{ client_id_minio }}"
      }]
      ]
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 204
  when: keycloak_platform_admin_response.status != 409

### Manage Group Assignments

- name: Get groups
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/groups"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: realm_groups

- name: "Set group id default_tenant."
  set_fact:
    group_default_tenant_id: "{{ realm_groups.json | json_query(query) }}"
  vars:
    query: "[?name== '{{ inv_idm.group.default_tenant_name }}'].id | [0]"

- name: Join Platform-Admin to Default-Tenant Group
  uri:
    method: PUT
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ REALM }}/users/{{ platform_user_id }}/groups/{{ group_default_tenant_id }}"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 204
  when: keycloak_platform_admin_response.status != 409   

